const mongoose = require('mongoose')

const personSchema = new mongoose.Schema({

    name: {
        type: String,
        trim: true,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    place: {
        type: String,
        trim: true,
        required: true
    },
    skill: {
        type: String,
        trim: true,
    }

}, {
    timestamps: true
})


const Person = mongoose.model('person', personSchema)
module.exports = Person