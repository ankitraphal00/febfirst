const mongoose = require('mongoose')

const countrySchema = new mongoose.Schema({

    nation: {
        type: String,
        required: true
    },
    case: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
})

const Country = mongoose.model('Country', countrySchema)
module.exports = Country