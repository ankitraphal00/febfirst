const mongoose = require('mongoose')

const test3Schema = new mongoose.Schema({
    name: {
        type: String,
        trim: true
    },
    age: {
        type: Number,
        required: true
    },
    numbers: [
        {
            type: Number,
            required: true
        }
    ],
    temp: [
        {
            type: Number,
            required: true
        }
    ]
})

module.exports = mongoose.model('test3', test3Schema)