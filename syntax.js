[
    {
        $group: {
            _id: {
                outputFieldName: '$fieldName',
                outputFieldName: '$fieldName'
            },
            total: {
                $sum: 1
            }
        }
    }, {
        $sort: {
            total: -1
        }
    }
]