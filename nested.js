const mongoose = require('mongoose');
const express = require('express')
const port = 3000
const app = express()

app.use(express.json())



mongoose.connect('mongodb://127.0.0.1:27017/testDB')
const User = require('./User')
const Address = require('./address')
const FriendAddress = require('./friendAddress')
const Animal = require('./animals')
const Test1 = require('./asyncseries')
const Test2 = require('./test2')
const Person = require('./persons')

const Test3 = require('./test3')

const v1 = require('./v1')
const v2 = require('./v2')
const v3 = require('./v3')

const async = require('async')

app.get('/nested', async (req, res) => {


    const data = await v1.aggregate([
        {
            $lookup: {
                from: 'v1addresses', // collection 2 name
                localField: 'address', //field name is 1st table
                foreignField: '_id', //Primary key of second table
                "pipeline": [
                    { "$project": { "_id": 0, "__v": 0, "createdAt": 0, "updatedAt": 0 } }
                ],
                as: 'Addresslookup',
            },
        },
        {
            $unwind: {
                path: "$Addresslookup",
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'countries', // collection 2 name
                localField: 'Addresslookup.country', //field name is 1st table
                foreignField: '_id', //Primary key of second table
                "pipeline": [
                    { "$project": { "_id": 0, "__v": 0, "createdAt": 0, "updatedAt": 0 } }
                ],
                as: 'countrylookup',
            },
        },
        {
            $project: { _id: 0, sex: 0, __v: 0 }
        }

    ])


    // {
    //     $project: { _id: 0, sex: 0, __v: 0 }
    // }

    // const data = await v1.find({})

    // const data = await v2.aggregate([
    //     {
    //         $lookup: {
    //             from: 'countries', // collection 2 name
    //             localField: 'country', //field name is 1st table
    //             foreignField: '_id', //Primary key of second table
    //             "pipeline": [
    //                 { "$project": { "_id": 0, "__v": 0, "createdAt": 0, "updatedAt": 0 } }
    //             ],
    //             as: 'countrylookup',
    //         },
    //     },
    //     {
    //         $project: { _id: 0, sex: 0, __v: 0 }
    //     }
    // ])




    res.send(data)







    // const user = await v1.create({
    //     name: 'user2',
    //     age: 45,
    //     address: '61fd0fdcb4bc555e5e13c704'
    // })

    // const user2 = await v1.create({
    //     name: 'user3',
    //     age: 22,
    //     address: '61fd100180cb7c772a5e8fd3'
    // })

    // res.send(user)





    // const address = await v2.create({
    //     country: '61fd0f729b975a10b0b1c584',
    //     street: 'gully gang'
    // })

    // res.send(address)




    // const country = await v3.create({
    //     nation: 'China',
    //     case: 0
    // })

    // res.send(country)


})























app.get('/map', async (req, res) => {

    const data = await Test3.aggregate([
        {
            $project: {
                tempInFahrenheit: {
                    $map: {
                        input: '$temp',// db field name
                        as: 'tempInCelsius',
                        in: {
                            $add: [{ $multiply: ['$$tempInCelsius', 9 / 5] }, 32]
                        }
                    }
                }
            }
        }
    ])

    res.send(data);


    // await Test3.create({
    //     name:'temp1',
    //     age:69,
    //     temp:[ 4, 12, 17 ]
    // })
    // await Test3.create({
    //     name:'temp2',
    //     age:69,
    //     temp:[ 14, 24, 11 ]
    // })
    // await Test3.create({
    //     name:'temp3',
    //     age:69,
    //     temp:[ 18, 6, 8 ]
    // })

})






app.get('/group', async (req, res) => {

    // const person = await Person.create({
    //     name: 'Spider Man',
    //     age: 43,
    //     place: 'CHD',
    //     skills: 'Leadership'
    // })

    // console.log(person)
    // res.send(person)


    // const person = await Person.find({ place: 'Mohali' })

    // // Group by single feild
    // const person = await Person.aggregate([
    //     {
    //         $group: {
    //             _id: '$place',
    //             total: {
    //                 $sum: 1
    //             }
    //         }
    //     }
    // ])


    const person = await Person.aggregate([
        {
            $group: {
                _id: {
                    place: '$place',
                    age: '$age'
                },
                totalage: {
                    $sum: '$age'
                },
                average: {
                    $avg: '$age'
                },
                outputSet: {
                    $addToSet: {
                        name: '$name',
                        age: '$age'
                    }
                },
                // mergedAge: {
                //     $mergeObjects: "$age"
                // },
                push: {
                    $push: {
                        name: '$name',
                        age: '$age'
                    },
                },
                total: {
                    $sum: 1
                }
            }
        },
        {
            $sort: {
                // '_id.place': -1 // Sort using group
                total: -1 // Sort using total
            }
        },
        //  {
        //     $project: {
        //         _id: 0,
        //         __v: 0,
        //         createdAt: 0,
        //         updatedAt: 0
        //     }
        // }
    ])


    res.send(person)

})











app.get('/test', async (req, res) => {

    // const test2Data = await Test2.create({
    //     dob: '31 Jan'
    // })

    // res.send(test2Data)

    // const test1Data = await Test1.create({
    //     name: 'B',
    //     age: '61e9386297f9781d6e7e26a3'
    // })

    // res.send(test1Data)

    try {
        const data = await Test1.aggregate([
            {
                $match: {
                    name: { $eq: 'A' }
                }
            },
            {
                $lookup: {
                    from: 'test2', // collection 2 name
                    localField: 'age', //field name is 1st table
                    foreignField: '_id', //Primary key of second table
                    "pipeline": [
                        { "$project": { "_id": 0, "__v": 0 } }
                    ],
                    as: 'lookup',
                }

            }, {
                $project: {
                    _id: 0,
                    __v: 0,
                    // method 2
                    // lookup:{
                    //     _id:0, __V:0
                    // }
                }
            }
        ])

        res.send(data)
    } catch (err) {
        console.log(err)
        res.send(err.message)
    }


})







app.get('/2', async (req, res) => {


    try {
        const fetch = await User.aggregate([{
            $match: {
                age: { $eq: 32 }
            }
        }, {
            $lookup: {
                from: 'friendaddresses',
                localField: 'friendAddress',
                foreignField: 'friendaddresses',
                as: 'lookupList'
            }
        }, {
            $project: { _id: 0, sex: 0, __v: 0 }
        }
        ])

        // console.log(JSON.stringify(fetch))
        res.send(fetch)

    } catch (err) {
        console.log(err)
        res.send(err)
    }

})




app.get('/', async (req, res) => {

    // Using populate
    // const fetch = await User.findById('61e6656a7395ed950c7d32b3')
    //     .populate({
    //         path: 'friends', model: 'User',
    //         populate: [{
    //             path: 'name',
    //             model: 'User',
    //             select: 'age name'
    //         }, {
    //             path: 'friendAddress',
    //             model: 'friendAddress',
    //             select: 'local'
    //         }],
    //     })


    //Using aggregations




    // const user = await User.find({
    //     // name: {
    //     //     first: { $eq: 'Dr' },
    //     //     last: { $eq: 'Banner' }
    //     // }
    //     age:{$eq : 12}
    // })

    // greater than 
    // const user = await User.find({
    //     age: {$gt: 12}
    // })


    // greater than || =
    // const user = await User.find({
    //     age: {$gte: 12}
    // })


    // less than 
    // const user = await User.find({
    //     age: {$lt: 22}
    // })

    // less than || =
    // const user = await User.find({
    //     age: {$lte: 12}
    // })


    // in  include only values in array
    // const user = await User.find({
    //     age: {$in: [12,40]}
    // })

    // nin 
    // const user = await User.find({
    //     age: { $nin: [12, 30] }
    // })

    // and 
    // const user = await User.find({
    //     age: { $eq: 12 },
    //     sex: "M"
    // })

    // const user = await User.find({
    //     $and:[
    //         {   
    //             age: {$eq: 12}
    //         },{
    //             sex: {$eq: "M"}
    //         }
    //     ]
    // })


    // or 
    // const user = await User.find({
    //     $or: [
    //         {
    //             age: { $eq: 31 }
    //         }, {
    //             sex: { $eq: 'F' }
    //         }
    //     ]
    // })

    // not
    // const user = await User.find({
    //     age: {$not: { $eq: 12 }}
    // })


    // exists
    // const user = await User.find({
    //     friends: {$exists: false}
    // })


    // match
    // const user = await User.aggregate(
    //     [
    //         {
    //             $match: {
    //                 $and: [
    //                     {
    //                         sex: 'M'
    //                     },
    //                     {
    //                         age: { $eq: 12 }
    //                     }
    //                 ]
    //             }
    //         }
    //     ]
    // )

    // const user = await User.aggregate([

    //     {
    //         $match: {
    //             $or: [
    //                 {
    //                     sex: 'M'
    //                 },
    //                 {
    //                     age: { $gt: 30 }
    //                 }
    //             ]
    //         }
    //     },
    // ])


    // const user = await User.aggregate([
    //     { $project: { _id: 0, age: 1 } }
    // ])

    // const user = await User.aggregate([
    //     {
    //         $project: {
    //             _id: 0,
    //             age: {
    //                 $cond: {
    //                     if: { $eq: [12, 'age'] },
    //                     then: { age: 0 },
    //                     else: { age: 1 }
    //                 }
    //             }
    //         }
    //     }
    // ])



    // console.log(user)
    // res.send(user)


    // const animal = await new Animal({
    //     name: 'dog'
    // })

    // animal.save()
    // console.log(animal)
    // res.send(animal)







})





app.listen(port, (error) => {
    if (error) {
        return console.log('0Error starting server \n' + error)
    }
    console.log('Server is up and running on port: ' + port)
})


// run()


// async function run() {

//     try {

//         // const user = new User({
//         //     // name: {
//         //     //     first: "MS",
//         //     //     last: "Dhoni"
//         //     // },
//         //     fullname: "Hello World",
//         //     age: 10,
//         //     sex: 'F',
//         //     email: 'k@k.com'
//         // })

//         const user = await User.create({
//             // name: {
//             //     first: "MS",
//             //     last: "Dhoni"
//             // },
//             fullname: "User 3",
//             age: 23,
//             sex: 'M',
//             email: 'user2@k.com',
//             // email: 'kk.com',
//             eggs: 7,
//             drink: 'tea',
//             friends: '61e5438d9842b86411322bab'
//         })

//         user.sex = 'M'


//         // await user.save()


//         console.log(user)
//         console.log(user.name.first + " " + user.name.last)

//     } catch (err) { console.log(err.message) }

// }


// async function run() {

//     try {
//         // const user = await User.findById('61e543983efbea8e2ab71706').populate('friends')
//         // const user2 = await User.findById('61e543983efbea8e2ab71706').populate('friends','name age')

//         // console.log(user)
//         // console.log(user2)

//         const users = await User.find({}).limit(1)
//         const users2 = await User.find({}).sort({ 'age': -1 })
//         const users3 = await User.find({}).skip(1)


//         // console.log(users)
//         // console.log(users2)
//         console.log(users3)



//     } catch (err) { console.log(err.message) }
// }



// async function run() {


//     // const user = await User.create({
//     //     fullname: 'Dr Banner',
//     //     age: 32,
//     //     sex: 'M',
//     //     friends: {
//     //         name: '61e665034cbc656c08769471',
//     //         friendAddress: '61e656994222cd63b7fe1689'
//     //     }
//     // })


//     // console.log(user)


//     // const address = await Address.create({
//     //     address: 'park',
//     //     country: 'China',
//     //     street: 'locak'
//     // })

//     // console.log(address)

//     const fetch = await User.findById('61e6656a7395ed950c7d32b3')
//         .populate({
//             path: 'friends',
//             populate: {
//                 path: 'name',
//             },
//         })
//         .populate({
//             path: 'friends',
//             populate: {
//                 path: 'friendAddress',
//             }
//         })



//     console.log(fetch)

//     // const friendAddress = await FriendAddress.create({
//     //     local: 'Kalka'
//     // })

//     // console.log(friendAddress)

// }