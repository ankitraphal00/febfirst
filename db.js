const mongoose = require('mongoose');

const connect = () => {
    console.log("mongo connected");
    return mongoose.connect("mongodb://localhost:27017/mapag");
}

module.exports = connect;