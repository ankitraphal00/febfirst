

// Function to increment counter
// function add() {
//     let counter = 0;
//     counter += 1;
//     return counter;
// }

// Call add() 3 times

function add() {
    let counter = 0;
    function add2() { counter += 1 };
    return counter;
}
add();
add();
add();
console.log(add());


