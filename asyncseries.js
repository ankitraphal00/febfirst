const async = require('async');

// async.series([
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'one');
//         }, 2000);
//     }, function (callback) {
//         setTimeout(function () {
//             callback(null, 'two');
//         }, 2000)
//     }, function (callback) {
//         setTimeout(function () {
//             // callback('This is error');
//             callback(null, 'three')
//         }, 2000)
//     }
// ], function (err, result) {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(result);
//     }

// })


//using objects
// async.series({
//     one: function (callback) {
//         setTimeout(function () {
//             callback(null, 1);
//         }, 2000)
//     },
//     two: function (callback) {
//         setTimeout(function () {
//             callback(null, 2);
//         })
//     }
// }, function (err, result) {
//     console.log(result);
// })

//using promises
// async.series([
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'one')
//         }, 2000)
//     },
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'two')
//         })
//     }
// ]).then(results => {
//     console.log(results);
// }).catch(err => {
//     console.log(err);
// })


// async.series({
//     one: function (callback) {
//         setTimeout(function () {

//             callback(null, 1);
//         }, 200);
//     },
//     two: function (callback) {
//         setTimeout(function () {

//             callback(null, 2);
//         }, 100);
//     }
// }).then(results => {
//     console.log(results);
// }).catch(err => {
//     console.log(err);
// });

// run();
// async function run() {
//     try {
//         let results = await async.series([
//             function (callback) {
//                 setTimeout(function () {
//                     callback(null, 'one')
//                 }, 200)
//             },
//             function (callback) {
//                 setTimeout(function () {
//                     callback(null, 'two');
//                 }, 200)
//             }
//         ])
//         console.log(results);

//     } catch (err) {
//         console.log(err);
//     }
// }


// async.parallel([
//     function (callback) {
//         setTimeout(function () {
//             callback(null, 'one');
//         }, 2000);
//     }, function (callback) {
//         setTimeout(function () {
//             callback(null, 'two');
//         }, 2000)
//     }, function (callback) {
//         setTimeout(function () {
//             // callback('This is error');
//             callback(null, 'three')
//         }, 2000)
//     }
// ], function (err, result) {
//     if (err) {
//         console.log(err);
//     } else {
//         console.log(result);
//     }

// })


// waterfall

// async.waterfall([
//     function (callback) {
//         callback(null, 'one', 'two');
//     },
//     function (arg1, arg2, callback) {
//         // arg1 now equals 'one' and arg2 now equals 'two'
//         callback(null, 'three');
//     },
//     function (arg1, callback) {
//         // arg1 now equals 'three'
//         callback(null, 'done');
//     }
// ], function (err, result) {
//     // result now equals 'done'
//     console.log(result);
// });


async.waterfall([
    myFirstFunction,
    mySecondFunction,
    myLastFunction,
], function (err, result) {

});
function myFirstFunction(callback) {
    callback(null, 'one', 'two');
}
function mySecondFunction(arg1, arg2, callback) {

    callback(null, 'three');
}
function myLastFunction(arg1, callback) {

    callback(null, 'done');
}