const mongoose = require('mongoose')

const test1Schema = new mongoose.Schema({
    name: {
        type: String,
    },
    age: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'test2'
    }
})

module.exports = mongoose.model('test1', test1Schema)